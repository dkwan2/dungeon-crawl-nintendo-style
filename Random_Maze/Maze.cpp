//---------------------------------------------------------------------
// Title: Maze.cpp
// Author: Darlene Kwan
// Course: Advanced Languages - CST 8333
// Lab: Section 312
// Assignment: Final Project
// Professor: Stan Pieda
// Date: Nov 26, 2012
// Description: Holds all data and functions of the Maze class
//---------------------------------------------------------------------

#include "Maze.h";
#include <sstream>
#include <fstream>

#define HIDDEN 0
#define VISIBLE 1
#define BMP_SIZE 20

// Function: setupMaze
// Description: Initializes data members of Maze class
void Maze::setupMaze(int width, int height, int movement, int chance) {
	row = width; // maze width
	col = height; // maze height
	max_moves = movement; // movement speed of mazebuilder
	spawn_chance = chance; // monster spawn percentage
	xBound = row - max_moves; // x boundary of maze
	yBound = col - max_moves; // y boundary of maze
	xPos = (rand() % xBound) + max_moves; // random x starting position
	yPos = (rand() % yBound) + max_moves; // random y starting position
	// load SDL images
	screen = SDL_SetVideoMode((width * BMP_SIZE), (height * BMP_SIZE) + (BMP_SIZE * 2), 32, SDL_SWSURFACE );
	mario = SDL_LoadBMP("mario.bmp");
	goomba = SDL_LoadBMP("goomba.bmp");
	mushroom = SDL_LoadBMP("1up.bmp");
	wall = SDL_LoadBMP("wall.bmp");
	pipe = SDL_LoadBMP("pipe.bmp");
	open = SDL_LoadBMP("open.bmp");
	// load text font
	font = TTF_OpenFont("minecraft_font.ttf", 20);
	// initialize color structure
	color.r = 255;
	color.g = 255;
	color.b = 255;

	// allocate memory for maze[col][row][visibility]
	maze = new int **[row];
	for (int i = 0; i < row; i++) {
		maze[i] = new int*[col];
		for (int j = 0; j < col; j++) {
			maze[i][j] = new int[2];
			maze[i][j][HIDDEN] = 0;
		}
	}
}

// Function: deleteMaze
// Description: Release allocated memory
void Maze::deleteMaze() {
	// free maze array
	for (int i = 0; i < row; i++) {
		for (int j = 0; j < col; j++) {
			delete [] maze[i][j];
		}
		delete [] maze[i];
	}
	delete [] maze;

	// free SDL surfaces
	SDL_FreeSurface(screen);
	SDL_FreeSurface(mario);
	SDL_FreeSurface(goomba);
	SDL_FreeSurface(mushroom);
	SDL_FreeSurface(wall);
	SDL_FreeSurface(pipe);
	SDL_FreeSurface(open);
	SDL_FreeSurface(player_health);
	SDL_FreeSurface(player_refills);
	TTF_CloseFont(font);
}

// Function: chckNeighbourWalls
// Description: returns number of walls around a given cell coordinate
int Maze::checkNeighbourWalls(int x, int y) {
	int counter = 0; // wall counter
	if (y > 0) { // maze boundary up
		if (maze[y-1][x][HIDDEN] == 0) { // check walls up
			counter++; // found wall
		}
	}
	if (x < (row - 1)) { // maze boundary right
		if (maze[y][x+1][HIDDEN] == 0) { // check walls right
			counter++; // found wall
		}
	}
	if (y < (col - 1)) { // maze boundary down
		if (maze[y+1][x][HIDDEN] == 0) { // check walls down
			counter++; // found wall
		}
	}
	if (x > 0) { // maze boundary left
		if (maze[y][x-1][HIDDEN] == 0) { // check walls left
			counter++; // found wall
		}
	}
	return counter; // number of walls found
}

// Function: buildMaze
// Description: Sets values in maze array
void Maze::buildMaze() {
	srand((int)time(NULL)); // initialize random seed
	int visited = 1; // number of cells visited by mazebuilder
	int total_cells = col * row; // total cells in maze
	int x = xPos; // mazebuilder x position
	int y = yPos; // mazebuilder y position
	maze[y][x][HIDDEN] = 2; // set player starting cell

	while (visited < total_cells) { // mazebuilder will visit all cells in maze
		switch ((int)((rand() % 4) + 1)) { // generate random number for mazebuilder move direction
		case 1: // NORTH
			if (y > (max_moves + 1)) { // check maze boundary (up)
				if (checkNeighbourWalls(x, y-1) >= 3) { // check cell ahead for surrounding walls
					buildUp(&x, &y); // clear cell ahead
				} else {
					if (y < (yBound - 1)) { // check maze boundary (down)
						buildDown(&x, &y); // backtrack
					}
				}
			}
			break;
		case 2: // EAST
			if (x < (xBound - 1)) { // check maze boundary (right)
				if (checkNeighbourWalls(x+1, y) >= 3) {
					buildRight(&x, &y);
				} else {
					if (x > (max_moves + 1)) {
						buildLeft(&x, &y);
					}
				}
			}
			break;
		case 3: // SOUTH
			if (y < (yBound - 1)) { // check maze boundary (down)
				if (checkNeighbourWalls(x, y+1) >= 3) {
					buildDown(&x, &y);
				} else {
					if (y > (max_moves + 1)) { 
						buildUp(&x, &y);
					}
				}
			}
			break;
		case 4: // WEST
			if (x > (max_moves + 1)) { // check maze boundary (left)
				if (checkNeighbourWalls(x-1, y) >= 3) {
					buildLeft(&x, &y);
				} else {
					if (x < (xBound - 1)) { 
						buildRight(&x, &y);
					}
				}
			}
			break;
		}
		if (maze[y][x][HIDDEN] != 2) { // if current cell is not the player
			if ((rand() % 100) <= spawn_chance) { // generate monster according to spawn_chance
				maze[y][x][HIDDEN] = 4; // set cell as monster
			} else if ((rand() % 100) <= (spawn_chance / 10)) { // generate 1up according to spawn_chance/4
				maze[y][x][HIDDEN] = 5; // set cell as 1up mushroom
			}
		}
		visited++; // increment cells visited by mazebuilder
	}
	maze[y][x][HIDDEN] = 3; // set cell as exit

	ofstream file; // create file object
	file.open("maze.txt"); // open file stream
	for (int i = 0; i < row; i++) { // iterate through maze array
		for (int j = 0; j < col; j++) {
			file << maze[i][j][HIDDEN]; // write generated maze data to file
		}
		file << "\n";
	}
	file.close(); // close file stream
}

// Function: buildUp
// Description: mazebuilder creates path in up direction
void Maze::buildUp(int *x, int *y) {
	int moves = 0; // used to track number of mazebuilder movements
	while (moves < max_moves) { // move mazebuilder max_moves number of times
		*y -= 1; // move mazebuilder up
		if (maze[*y][*x][HIDDEN] != 2) { // check if current cell is player
			maze[*y][*x][HIDDEN] = 1; // set current cell as open path
			moves++; // increment number of moves made by mazebuilder
		}
	}
}

// Function: buildRight
// Description: mazebuilder creates path in right direction
void Maze::buildRight(int *x, int *y) {
	int moves = 0;
	while (moves < max_moves) {
		*x += 1; // move mazebuilder right
		if (maze[*y][*x][HIDDEN] != 2) {
			maze[*y][*x][HIDDEN] = 1;
			moves++;
		}
	}
}

// Function: buildDown
// Description: mazebuilder creates path in down direction
void Maze::buildDown(int *x, int *y) {
	int moves = 0;
	while (moves < max_moves) {
		*y += 1; // move mazebuilder down
		if (maze[*y][*x][HIDDEN] != 2) {
			maze[*y][*x][HIDDEN] = 1;
			moves++;
		}
	}
}

// Function: buildLeft
// Description: mazebuilder creates path in left direction
void Maze::buildLeft(int *x, int *y) {
	int moves = 0;
	while (moves < max_moves) {
		*x -= 1; // move mazebuilder left
		if (maze[*y][*x][HIDDEN] != 2) {
			maze[*y][*x][HIDDEN] = 1;
			moves++;
		}
	}
}

// Function: drawMaze
// Description: Draws images corresponding to maze array values
void Maze::drawMaze(Player p) {
	int pHealth = p.getHealth(); // player health
	int pRefills = p.getRefills(); // health refills
	SDL_Rect text_offset; // structure for text coordinates
	SDL_Rect draw_offset; // structure for drawing coordinates
	char message[256]; // text
	message[0] = '\0'; // set char array to null characters

	SDL_FillRect(screen, NULL, 0x000000); // black out screen
	SDL_Flip(screen); // update screen

	strcpy(message, "Health: "); // copy string into text
	itoa(pHealth, message + 8, 10); // copy player health into text
	player_health = TTF_RenderText_Solid(font, message, color); // create health text
	strcpy(message, "Refills: "); // copy string into text
	itoa(pRefills, message + 9, 10); // copy health refills into text
	player_refills = TTF_RenderText_Solid(font, message, color); // create health refill text
	text_offset.y = BMP_SIZE/2; // set x coordinate for text position
	text_offset.x = BMP_SIZE/2; // set y coordinate for text position
	SDL_BlitSurface(player_health, NULL, screen, &text_offset); // add player health text to screen
	text_offset.x = (col / 3) * BMP_SIZE; // position next text at 1/3 screen width
	SDL_BlitSurface(player_refills, NULL, screen, &text_offset); // add health refill text to screen

	for (int i = 0; i < row; i++) { // iterate through maze array
		draw_offset.y = (i * BMP_SIZE) + (BMP_SIZE * 2); // update drawing y coordinate
		for (int j = 0; j < col; j++) {
			draw_offset.x = j * BMP_SIZE; // update drawing x coordinate
			if (maze[i][j][VISIBLE] == 0) { // draw wall
				SDL_BlitSurface(wall, NULL, screen, &draw_offset);
			} else if (maze[i][j][VISIBLE] == 1) { // draw open path
				SDL_BlitSurface(open, NULL, screen, &draw_offset);
			} else if (maze[i][j][VISIBLE] == 2) { // draw player
				SDL_BlitSurface(mario, NULL, screen, &draw_offset);
			} else if (maze[i][j][VISIBLE] == 3) { // draw end
				SDL_BlitSurface(pipe, NULL, screen, &draw_offset);
			} else if (maze[i][j][VISIBLE] == 4) { // draw monster
				SDL_BlitSurface(goomba, NULL, screen, &draw_offset);
			} else if (maze[i][j][VISIBLE] == 5) { // draw 1up
				SDL_BlitSurface(mushroom, NULL, screen, &draw_offset);
			} else {
				SDL_BlitSurface(open, NULL, screen, &draw_offset); // draw open path
			}
		}
	}
	SDL_Flip(screen); // update screen
}

// Function: move
// Description: Updates player position in maze given movement direction
int Maze::move(int movement) {
	switch(movement) {
	case 1: // UP
		if (yPos > 0) { // check maze boundary (up)
			if (maze[yPos-1][xPos][HIDDEN] == 1) { // check for open path
				moveUp(); // move player
			} else if (maze[yPos-1][xPos][HIDDEN] == 3) { // check for exit
				return 1; // return code for exit detection
			} else if (maze[yPos-1][xPos][HIDDEN] == 4) { // check for monster
				moveUp(); // move player
				return 2; // return code for monster detection
			} else if (maze[yPos-1][xPos][HIDDEN] == 5) { // check for 1up mushroom
				moveUp(); // move player
				return 3; // return code for 1up mushroom detection
			}
		}
		break;
	case 2: // RIGHT
		if (xPos < (row - 1)) { // check maze boundary (right)
			if (maze[yPos][xPos+1][HIDDEN] == 1) {
				moveRight();
			} else if (maze[yPos][xPos+1][HIDDEN] == 3) {
				return 1;
			} else if (maze[yPos][xPos+1][HIDDEN] == 4) {
				moveRight();
				return 2;
			} else if (maze[yPos][xPos+1][HIDDEN] == 5) {
				moveRight();
				return 3;
			}
		}
		break;
	case 3: // DOWN
		if (yPos < (col - 1)) { // check maze boundary (down)
			if (maze[yPos+1][xPos][HIDDEN] == 1) {
				moveDown();
			} else if (maze[yPos+1][xPos][HIDDEN] == 3) {
				return 1;
			} else if (maze[yPos+1][xPos][HIDDEN] == 4) {
				moveDown();
				return 2;
			} else if (maze[yPos+1][xPos][HIDDEN] == 5) {
				moveDown();
				return 3;
			}
		}
		break;
	case 4: // LEFT
		if (xPos > 0) { // check maze boundary (left)
			if (maze[yPos][xPos-1][HIDDEN] == 1) {
				moveLeft();
			} else if (maze[yPos][xPos-1][HIDDEN] == 3) {
				return 1;
			} else if (maze[yPos][xPos-1][HIDDEN] == 4) {
				moveLeft();
				return 2;
			} else if (maze[yPos][xPos-1][HIDDEN] == 5) {
				moveLeft();
				return 3;
			}
		}
		break;
	}
	return 0; // return zero if nothing detected
}

// Function: moveUp
// Description: move player up
void Maze::moveUp() {
	maze[yPos][xPos][HIDDEN] = 1; // set current cell as open path
	yPos -= 1; // move player to next cell
	maze[yPos][xPos][HIDDEN] = 2; // set current cell as player
}

// Function: moveRight
// Description: move player right
void Maze::moveRight() {
	maze[yPos][xPos][HIDDEN] = 1;
	xPos += 1;
	maze[yPos][xPos][HIDDEN] = 2;
}

// Function: moveDown
// Description: move player down
void Maze::moveDown() {
	maze[yPos][xPos][HIDDEN] = 1;
	yPos += 1;
	maze[yPos][xPos][HIDDEN] = 2;
}

// Function: moveLeft
// Description: move player left
void Maze::moveLeft() {
	maze[yPos][xPos][HIDDEN] = 1;
	xPos -= 1;
	maze[yPos][xPos][HIDDEN] = 2;
}


// Function: updateView
// Description: Update visible cells in maze[][][]
void Maze::updateView() {
	int viewDistance = 3; // player view distance
	maze[yPos][xPos][VISIBLE] = maze[yPos][xPos][HIDDEN]; // set player cell as visible
	
	// set adjacent cells to player as visible
	for (int i = 1; i <= viewDistance; i++) { 
		if (yPos > (i - 1)) {
			maze[yPos-i][xPos][VISIBLE] = maze[yPos-i][xPos][HIDDEN];
		}
		if (xPos < (row - i)) {
			maze[yPos][xPos+i][VISIBLE] = maze[yPos][xPos+i][HIDDEN];
		}
	}
	for (int i = 1; i <= viewDistance; i++) {
		if (yPos < (col - i)) {
			maze[yPos+i][xPos][VISIBLE] = maze[yPos+i][xPos][HIDDEN];
		}
		if (xPos > (i - 1)) {
			maze[yPos][xPos-i][VISIBLE] = maze[yPos][xPos-i][HIDDEN];
		}
	}
	for (int i = 1; i <= viewDistance; i++) {
		if (yPos < (col - i)) {
			maze[yPos+i][xPos][VISIBLE] = maze[yPos+i][xPos][HIDDEN];
		}
		if (xPos > (i - 1)) {
			maze[yPos][xPos-i][VISIBLE] = maze[yPos][xPos-i][HIDDEN];
		}
	}
	for (int i = 1; i <= viewDistance; i++) {
		if (yPos > (i - 1)) {
			maze[yPos-i][xPos][VISIBLE] = maze[yPos-i][xPos][HIDDEN];
		}
		if (xPos < (row - i)) {
			maze[yPos][xPos+i][VISIBLE] = maze[yPos][xPos+i][HIDDEN];
		}
	}
}