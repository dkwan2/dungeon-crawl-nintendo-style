//---------------------------------------------------------------------
// Title: main.cpp
// Author: Darlene Kwan
// Course: Advanced Languages - CST 8333
// Lab: Section 312
// Assignment: Final Project
// Professor: Stan Pieda
// Date: Nov 26, 2012
// Description: Execution point of Random Maze Dungeon Crawler
//---------------------------------------------------------------------

#include <conio.h>;
#include "Maze.cpp";
#include "Player.h";
#include "SDL_ttf.h";
#include "SDL_mixer.h";

#define SOUNDFX_CHANNEL -1

// declare Sound_Effect members
struct Sound_Effect {
    Mix_Chunk* mushroom;
	Mix_Chunk* grow;
	Mix_Chunk* ouch;
	Mix_Chunk* pause;
	Mix_Chunk* game_over; 
	Mix_Chunk* victory;
};

// declare function prototypes in main.cpp
void startGame(int width, int height, int* difficulty, int* play_music, Mix_Music* music, Sound_Effect sound_effect);
void displayMenu(int width, int height, int *difficulty, int* play_music, Mix_Music* music);
void displayEnd(int width, int height, bool win);

// Function: main
// Description:	Entry point of the program
int main(int argc, char *argv[]) {

	// maze size
	int width = 30;
	int height = 30;
	// difficulty level
	int difficulty = 1;
	// music playing
	int play_music = 1;

	// audio settings
	int audio_rate = 22050;
	Uint16 audio_format = AUDIO_S16; /* 16-bit stereo */
	int audio_channels = 2;
	int audio_buffers = 4096;

	SDL_Init(SDL_INIT_EVERYTHING); // start SDL
	SDL_Init(SDL_INIT_AUDIO); // start SDL_mixer
	TTF_Init(); // start SDL_ttf
	SDL_WM_SetCaption("Random Maze Dungeon Crawl - Created by: Darlene Kwan", NULL); // set the window caption 
	Mix_OpenAudio(audio_rate, audio_format, audio_channels, audio_buffers); // open audio channel
	
	// load audio
	Mix_Music* music = NULL;
	music = Mix_LoadMUS("theme_song.wav");
	Sound_Effect sound_effect;
	sound_effect.mushroom = Mix_LoadWAV("1up.wav");
	sound_effect.grow = Mix_LoadWAV("grow.wav");
	sound_effect.ouch = Mix_LoadWAV("ouch.wav");
	sound_effect.pause = Mix_LoadWAV("pause.wav");
	sound_effect.game_over = Mix_LoadWAV("game_over.wav");
	sound_effect.victory = Mix_LoadWAV("victory.wav");

	while(true) {
		if (play_music == 1) { // check if sound is 'ON'
			Mix_HaltChannel(SOUNDFX_CHANNEL); // stop any sound playing on channel -1
			Mix_PlayMusic(music, -1); // play background music (infinite loop)
		}
		try {
			displayMenu(width, height, &difficulty, &play_music, music); // display main menu
			startGame(width, height, &difficulty, &play_music, music, sound_effect); // start game instance
		} catch (char *str) {
			cout << "Exception: " << str << endl;
		}
	}
	
	// free audio resources
	Mix_FreeMusic(music);
	Mix_FreeChunk(sound_effect.mushroom);
	Mix_FreeChunk(sound_effect.grow);
	Mix_FreeChunk(sound_effect.ouch);
	Mix_FreeChunk(sound_effect.pause);
	Mix_FreeChunk(sound_effect.game_over);
	Mix_FreeChunk(sound_effect.victory);
	Mix_CloseAudio(); // quit SDL_mixer
	TTF_Quit(); // quit SDL_ttf 
	SDL_Quit(); // quit SDL

	return 0;
}

// Function: startGame
// Description:	Game instance
void startGame(int width, int height, int* difficulty, int* play_music, Mix_Music* music, Sound_Effect sound_effect) {

	// initialize variables
	int player_health = 100;
	int health_refills = 3;
	int move_speed = 2;
	int monster_spawn = 20;
	int quit = 0;

	Player myPlayer; // instantiate player object
	myPlayer.setupPlayer(player_health, health_refills); // set player object with player_health and health_refills
	Maze myMaze; // instantiate maze object
	myMaze.setupMaze(width, height, move_speed, monster_spawn); // set maze object with maze_width, maze_height, move_speed (of mazebuilder), monster_spawn percentage
	myMaze.buildMaze(); // build the maze
	myMaze.updateView(); // update player field of view
	myMaze.drawMaze(myPlayer); // draw maze

	SDL_Event event; // SDL event structure
	while (quit == 0) { // loop while game is running
		int rc = 0; // initialize return code
		if (SDL_PollEvent(&event)) { // wait for event
			if (event.type == SDL_KEYDOWN) { // event is key press
				switch(event.key.keysym.sym) { 
				case SDLK_UP: // up arrow key
					rc = myMaze.move(1); // store return code for player move up
					break;
				case SDLK_RIGHT: // right arrow key
					rc = myMaze.move(2); // store return code for player move right
					break;
				case SDLK_DOWN:  // down arrow key
					rc = myMaze.move(3); // store return code for player move down
					break; 
				case SDLK_LEFT:  // left arrow key
					rc = myMaze.move(4); // store return code for player move left
					break; 
				case SDLK_h: // H key
					if (*play_music == 1) {
						Mix_PlayChannel(SOUNDFX_CHANNEL, sound_effect.grow, 0); // play health refill sound
					}
					myPlayer.healthRefill(); // use health refill
					break;
				case SDLK_ESCAPE: // esc key
					exit(0);
					break;
				default:
					if (*play_music == 1) {
						Mix_PlayChannel(SOUNDFX_CHANNEL, sound_effect.pause, 0); // play pause sound
					}
					displayMenu(width, height, difficulty, play_music, music); // display main menu
					break;
				} 
			} else if (event.type == SDL_QUIT) { // user has Xed out the window
				exit(0);
			} 
			if (rc == 1) { // found maze exit
				if (*play_music == 1) {
					Mix_HaltMusic(); // stop background music
					Mix_PlayChannel(SOUNDFX_CHANNEL, sound_effect.victory, 0); // play victory sound
				}
				displayEnd(width, height, true); // display win message
				quit = 1; // exit loop
			} 
			if (rc == 2) { // monster encounter
				if (*play_music == 1) {
					Mix_PlayChannel(SOUNDFX_CHANNEL, sound_effect.ouch, 0 ); // play ouch sound
				}
				int monster_health = ((rand() % 100) + 1) * (*difficulty); // generate monster health using difficulty level
				myPlayer.fight(monster_health); // fight sequence with monster health
			}
			if (rc == 3) { // found 1up mushroom
				if (*play_music == 1) {
					Mix_PlayChannel(SOUNDFX_CHANNEL, sound_effect.mushroom, 0); // play 1up sound
				}
				myPlayer.addRefill(); // add 1 to health refill
			}
			if (myPlayer.getHealth() <= 0) { // player is dead
				if (*play_music == 1) {
					Mix_HaltMusic(); // stop background music
					Mix_PlayChannel(SOUNDFX_CHANNEL, sound_effect.game_over, 0); // play game over sound
				}
				displayEnd(width, height, false); // display lose message
				quit = 1; // exit loop
			}

			myMaze.updateView(); // update player view
			myMaze.drawMaze(myPlayer); // re-draw maze
		}
	}
	
	myMaze.deleteMaze(); // free memory
}

// Function: displayMenu
// Description:	Displays main menu
void displayMenu(int width, int height, int* difficulty, int* play_music, Mix_Music* music) {
	TTF_Font *font = TTF_OpenFont("minecraft_font.ttf", 15);
	SDL_Color color;
	color.r = 255;
	color.g = 255;
	color.b = 255;
	SDL_Surface* text = NULL;
	SDL_Surface* menu = NULL;
	menu = SDL_SetVideoMode((width*BMP_SIZE), (height*BMP_SIZE) + (BMP_SIZE/2), 32, SDL_SWSURFACE );
	SDL_Rect offset; 
	offset.x = BMP_SIZE; 
	offset.y = BMP_SIZE;
	SDL_Rect difficulty_offset;
	difficulty_offset.x = (width*BMP_SIZE)/4;
	difficulty_offset.y = 320;
	SDL_Rect difficulty_rect;
	difficulty_rect.x = difficulty_offset.x;
	difficulty_rect.y = difficulty_offset.y;
	difficulty_rect.w = (width*BMP_SIZE)/4;
	difficulty_rect.h = BMP_SIZE;
	SDL_Rect music_offset;
	music_offset.x = (width*BMP_SIZE)/4;
	music_offset.y = 360;
	SDL_Rect music_rect;
	music_rect.x = music_offset.x;
	music_rect.y = music_offset.y;
	music_rect.w = (width*BMP_SIZE)/4;
	music_rect.h = BMP_SIZE;

	text = TTF_RenderText_Solid(font, "RANDOM MAZE DUNGEON CRAWL", color);
	SDL_BlitSurface(text, NULL, menu, &offset);
	offset.y += BMP_SIZE;
	text = TTF_RenderText_Solid(font, "-----------------------", color);
	SDL_BlitSurface(text, NULL, menu, &offset);
	offset.y += BMP_SIZE*2;
	text = TTF_RenderText_Solid(font, "OBJECTIVES :", color);
	SDL_BlitSurface(text, NULL, menu, &offset);
	offset.y += BMP_SIZE;
	text = TTF_RenderText_Solid(font, "- Find the exit in the maze", color);
	SDL_BlitSurface(text, NULL, menu, &offset);
	offset.y += BMP_SIZE;
	text = TTF_RenderText_Solid(font, "- Be careful of monsters", color);
	SDL_BlitSurface(text, NULL, menu, &offset);
	offset.y += BMP_SIZE;
	text = TTF_RenderText_Solid(font, "- Watch your health", color);
	SDL_BlitSurface(text, NULL, menu, &offset);
	offset.y += BMP_SIZE*2;
	text = TTF_RenderText_Solid(font, "CONTROLS :", color);
	SDL_BlitSurface(text, NULL, menu, &offset);
	offset.y += BMP_SIZE;
	text = TTF_RenderText_Solid(font, "[ arrow keys ] to move", color);
	SDL_BlitSurface(text, NULL, menu, &offset);
	offset.y += BMP_SIZE;
	text = TTF_RenderText_Solid(font, "[ h ] to use health refill", color);
	SDL_BlitSurface(text, NULL, menu, &offset);
	offset.y += BMP_SIZE;
	text = TTF_RenderText_Solid(font, "[ d ] to toggle difficulty", color);
	SDL_BlitSurface(text, NULL, menu, &offset);
	offset.y += BMP_SIZE;
	text = TTF_RenderText_Solid(font, "[ m ] to toggle music on / off", color);
	SDL_BlitSurface(text, NULL, menu, &offset);
	offset.y += BMP_SIZE;
	text = TTF_RenderText_Solid(font, "[ esc ] to quit", color);
	SDL_BlitSurface(text, NULL, menu, &offset);
	offset.y += BMP_SIZE*2;
	text = TTF_RenderText_Solid(font, "DIFFICULTY :", color);
	SDL_BlitSurface(text, NULL, menu, &offset);
	offset.y += BMP_SIZE*2;
	text = TTF_RenderText_Solid(font, "SOUND :", color);
	SDL_BlitSurface(text, NULL, menu, &offset);
	
	switch(*difficulty) {
	case 1: // difficulty 1
		text = TTF_RenderText_Solid(font, "EASY", color);
		break;
	case 2: // difficulty 2
		text = TTF_RenderText_Solid(font, "MEDIUM", color);
		break;
	case 3: // difficulty 3
		text = TTF_RenderText_Solid(font, "HARD", color);
		break;
	}
	SDL_BlitSurface(text, NULL, menu, &difficulty_offset);

	if (*play_music == 1) { // music on
		text = TTF_RenderText_Solid(font, "ON", color);
	} else { // music off
		text = TTF_RenderText_Solid(font, "OFF", color);
	}
	SDL_BlitSurface(text, NULL, menu, &music_offset);

	offset.y = (height*BMP_SIZE)-BMP_SIZE;
	text = TTF_RenderText_Solid(font, "Press any key to continue . . .", color);
	SDL_BlitSurface(text, NULL, menu, &offset);

	SDL_Flip(menu);

	SDL_Event event;
    while (SDL_WaitEvent(&event) >= 0) { // wait for event
		if (event.type == SDL_KEYDOWN) { // event is key press
			switch(event.key.keysym.sym) {
			case SDLK_ESCAPE: // escape key
				exit(0);
				break;
			case SDLK_d: // D key
				if (*difficulty < 3) {
					*difficulty += 1;
				} else {
					*difficulty = 1;
				}
				switch(*difficulty) {
				case 1: // difficulty 1
					text = TTF_RenderText_Solid(font, "EASY", color);
					break;
				case 2: // difficulty 2
					text = TTF_RenderText_Solid(font, "MEDIUM", color);
					break;
				case 3: // difficulty 3
					text = TTF_RenderText_Solid(font, "HARD", color);
					break;
				}
				SDL_FillRect(menu, &difficulty_rect, 0x000000);
				SDL_BlitSurface(text, NULL, menu, &difficulty_offset);
				SDL_Flip(menu);
				break;
			case SDLK_m: // M key
				if (*play_music == 1) { // music off
					*play_music = 0;
					Mix_HaltMusic();
					text = TTF_RenderText_Solid(font, "OFF", color);
				} else { // music on
					*play_music = 1;
					Mix_PlayMusic(music, -1);
					text = TTF_RenderText_Solid(font, "ON", color);
				}
				SDL_FillRect(menu, &music_rect, 0x000000);
				SDL_BlitSurface(text, NULL, menu, &music_offset);
				SDL_Flip(menu);
				break;
			default: // any key
				SDL_FreeSurface(menu);
				SDL_FreeSurface(text);
				TTF_CloseFont(font);
				return;
				break;
			}
		} else if (event.type == SDL_QUIT) { // user has Xed out the window
			exit(0);
		} 
	}
}

// Function: displayEnd
// Description:	Display end message (win or lose)
void displayEnd(int width, int height, bool win) {
	TTF_Font *font = TTF_OpenFont("minecraft_font.ttf", 15);
	SDL_Color color;
	color.r = 255;
	color.g = 255;
	color.b = 255;
	SDL_Surface* text = NULL;
	SDL_Surface* menu = NULL;
	menu = SDL_SetVideoMode((width*BMP_SIZE), (height*BMP_SIZE) + (BMP_SIZE/2), 32, SDL_SWSURFACE );
	SDL_Rect offset; 
	offset.x = (width*BMP_SIZE)/4; 
	offset.y = (height*BMP_SIZE)/4;
	SDL_Surface* message;

	if (win) { // win message
		message = SDL_LoadBMP("victory.bmp");
		SDL_BlitSurface(message, NULL, menu, &offset);
	} else { // lose message
		message = SDL_LoadBMP("gameover.bmp");
		SDL_BlitSurface(message, NULL, menu, &offset);
	}
	offset.x = BMP_SIZE; 
	offset.y = (height*BMP_SIZE) - (BMP_SIZE*2);
	text = TTF_RenderText_Solid(font, "Press any key to play again . . .", color);
	SDL_BlitSurface(text, NULL, menu, &offset);
	offset.y += BMP_SIZE;
	text = TTF_RenderText_Solid(font, "Press [Esc] to quit", color);
	SDL_BlitSurface(text, NULL, menu, &offset);

	SDL_Flip(menu);

	SDL_Event event;
    while (SDL_WaitEvent(&event) >= 0) { // wait for event
		if (event.type == SDL_KEYDOWN) { // event is key press
			switch(event.key.keysym.sym) {
			case SDLK_ESCAPE: // escape key
				exit(0);
				break;
			default: // any key
				SDL_FreeSurface(message);
				SDL_FreeSurface(menu);
				SDL_FreeSurface(text);
				TTF_CloseFont(font);
				return;
				break;
			}
		} else if (event.type == SDL_QUIT) { // user has Xed out the window
			exit(0);
		} 
	}
}