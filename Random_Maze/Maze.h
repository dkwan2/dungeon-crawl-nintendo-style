//---------------------------------------------------------------------
// Title: Maze.h
// Author: Darlene Kwan
// Course: Advanced Languages - CST 8333
// Lab: Section 312
// Assignment: Final Project
// Professor: Stan Pieda
// Date: Nov 26, 2012
// Description: Declaration of Maze class
//---------------------------------------------------------------------

#ifndef _MAZE_H_
#define _MAZE_H_

#include <iostream>;
#include <time.h>;
#include "sdl.h";
#include "SDL_ttf.h";
#include "Player.h";
using namespace std;

// Class: Maze
// Description: Declares a Maze object
class Maze {
	// define data members of class
	int col, row;
	int max_moves;
	int spawn_chance;
	int xBound, yBound;
	int xPos, yPos;
	int ***maze;
	SDL_Surface* screen;
	SDL_Surface* mario;
	SDL_Surface* goomba;
	SDL_Surface* mushroom;
	SDL_Surface* wall;
	SDL_Surface* pipe;
	SDL_Surface* open;
	SDL_Surface* player_health;
	SDL_Surface* player_refills;
	TTF_Font *font;
	SDL_Color color;
public:
	// define public members of class
	void setupMaze(int, int, int, int);
	void deleteMaze();
	int checkNeighbourWalls(int, int);
	void buildMaze();
	void buildUp(int*, int*);
	void buildRight(int*, int*);
	void buildDown(int*, int*);
	void buildLeft(int*, int*);
	void drawMaze(Player);
	int move(int);
	void moveUp();
	void moveRight();
	void moveDown();
	void moveLeft();
	void updateView();
};

#endif