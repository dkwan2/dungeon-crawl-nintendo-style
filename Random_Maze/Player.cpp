//---------------------------------------------------------------------
// Title: Player.cpp
// Author: Darlene Kwan
// Course: Advanced Languages - CST 8333
// Lab: Section 312
// Assignment: Final Project
// Professor: Stan Pieda
// Date: Nov 26, 2012
// Description: Holds all data and functions of the Player class
//---------------------------------------------------------------------

#include "Player.h";

// Function: setupPlayer
// Description: Initializes data members of Player class
void Player::setupPlayer(int player_health, int health_refills) {
	health = player_health; // player health
	refills = health_refills; // health refills
}

// Function: getHealth
// Description: Returns amount of player health
int Player::getHealth() {
	return health;
}

// Function: getRefills
// Description: Returns number of health refills
int Player::getRefills() {
	return refills;
}

// Function: fight
// Description: Fight sequence between player and monster
void Player::fight(int monster_health) {
	int monster = monster_health; // store monster health
	while ((monster > 0) && (health > 0)) { // continue fighting if monster and player still alive
		health -= rand() % 10; // reduce player health
		monster -= rand() % 20; // reduce monster health
		if (health < 0) {
			health = 0; // set player health to zero
		}
		if (monster < 0) {
			monster = 0; // set monster health to zero
		}
	}
}

// Function: healthRefill
// Description: Use a health refill to restore player health by 50
void Player::healthRefill() {
	if (refills > 0) { // check number of health refills
		health += 50; // increase player health
		if (health > 100) {
			health = 100; // cap out health at 100
		}
		refills--; // decrement number of health refills
	}
}

// Function: addRefill
// Description: Increment the number of health refills
void Player::addRefill() {
	refills++;
}