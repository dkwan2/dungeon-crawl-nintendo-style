//---------------------------------------------------------------------
// Title: Player.h
// Author: Darlene Kwan
// Course: Advanced Languages - CST 8333
// Lab: Section 312
// Assignment: Final Project
// Professor: Stan Pieda
// Date: Nov 26, 2012
// Description: Declaration of Player class
//---------------------------------------------------------------------

#ifndef _PLAYER_H_
#define _PLAYER_H_

#include <iostream>;
using namespace std;

// Class: Player
// Description: Declares a Player object
class Player {
	// define data members of class
	int health;
	int refills;
public:
	// define public members of class
	void setupPlayer(int, int);
	int getHealth();
	int getRefills();
	void fight(int);
	void healthRefill();
	void addRefill();
};

#endif